package com.wipro.technovation.retailapparel;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class MyCart extends AppCompatActivity {

    //All assets related to Apparel App
    final File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Apparel");


    //UI Assets
    private ImageView ivCartItem1, ivCartItem2, ivCartItem3, ivCartDelivery;
    private Button btCheckout;

    String filename = "";
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        ivCartItem1 = (ImageView) findViewById(R.id.ivCartItem1);
        ivCartItem2 = (ImageView) findViewById(R.id.ivCartItem2);
        ivCartItem3 = (ImageView) findViewById(R.id.ivCartItem3);
        ivCartDelivery = (ImageView) findViewById(R.id.ivCartDelivery);
        ivCartDelivery.setVisibility(View.GONE);

        ivCartDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyCart.this);
                builder.setCancelable(false);
                builder.setTitle("Almost There... ");
                builder.setMessage("Your items will be delivered after successful payment ").setPositiveButton("Pay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        btCheckout = (Button) findViewById(R.id.btCheckout);
        btCheckout.setVisibility(View.VISIBLE);

        setItem1("cart_item_1.png");
        setItem2("cart_item_2.png");
        setItem3("cart_item_3.png");
    }

    private void setItem1(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivCartItem1.setImageBitmap(bitmap);
    }

    private void setItem2(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivCartItem2.setImageBitmap(bitmap);
    }

    private void setItem3(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivCartItem3.setImageBitmap(bitmap);
    }

    public void showCheckout(View v) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/checkoutScreen.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivCartDelivery.setImageBitmap(bitmap);
        ivCartDelivery.setVisibility(View.VISIBLE);

        ivCartItem1.setVisibility(View.GONE);
        ivCartItem2.setVisibility(View.GONE);
        ivCartItem3.setVisibility(View.GONE);

        btCheckout.setText("Deliver all Items");

    }

}
