package com.wipro.technovation.retailapparel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;

public class ProductList extends AppCompatActivity {

    //All assets related to Apparel App
    final File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Apparel");

    private FloatingActionButton fab;

    //UI Assets
    private ImageView ivProduct1, ivProduct2, ivProduct3, ivProduct4, ivProduct5, ivProduct6;

    String filename = "";
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        //Cart floating button
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductList.this, MyCart.class));
            }
        });

        ivProduct1 = (ImageView) findViewById(R.id.ivProduct1);
        ivProduct2 = (ImageView) findViewById(R.id.ivProduct2);
        ivProduct3 = (ImageView) findViewById(R.id.ivProduct3);
        ivProduct4 = (ImageView) findViewById(R.id.ivProduct4);
        ivProduct5 = (ImageView) findViewById(R.id.ivProduct5);
        ivProduct6 = (ImageView) findViewById(R.id.ivProduct6);

        filename = retailApparelFileRoot.getAbsolutePath() + "/women_recom_1.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct1.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct1.setImageBitmap(bitmap);
        ivProduct1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductList.this, WomenDress.class));
            }
        });

        filename = retailApparelFileRoot.getAbsolutePath() + "/women_recom_2.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct2.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct2.setImageBitmap(bitmap);

        filename = retailApparelFileRoot.getAbsolutePath() + "/new_arrival_1.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct3.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct3.setImageBitmap(bitmap);

        filename = retailApparelFileRoot.getAbsolutePath() + "/new_arrival_2.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct4.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct4.setImageBitmap(bitmap);

        filename = retailApparelFileRoot.getAbsolutePath() + "/you_like_1.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct5.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct5.setImageBitmap(bitmap);

        filename = retailApparelFileRoot.getAbsolutePath() + "/you_like_2.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct6.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct6.setImageBitmap(bitmap);
    }
}
