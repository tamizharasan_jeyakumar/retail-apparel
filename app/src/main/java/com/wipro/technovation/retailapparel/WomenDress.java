package com.wipro.technovation.retailapparel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class WomenDress extends AppCompatActivity {

    //All assets related to Apparel App
    final File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Apparel");

    //UI Assets
    private ImageView ivDress, ivSimilarDress, ivCompleteLook;
    private FloatingActionButton fab;

    String filename = "";
    Bitmap bitmap;

    private Animation fab_shake;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_women_dress);

        ivDress = (ImageView) findViewById(R.id.ivDress);
        ivSimilarDress = (ImageView) findViewById(R.id.ivSimilarDress);
        ivCompleteLook = (ImageView) findViewById(R.id.ivCompleteLook);

        ivCompleteLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(getString(R.string.debug_tag), "Item is scarf");
                setChosenProduct("scarf_1.png");
                setSimillarProduct("scarf_2.png");
                setCompleteLook("scarf_3.png");
            }
        });

        //Cart floating button
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WomenDress.this, MyCart.class));
            }
        });

        ivDress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(WomenDress.this, "Item Added to cart", Toast.LENGTH_SHORT).show();
                fab_shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                fab.startAnimation(fab_shake);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fab.clearAnimation();
                    }
                }, 2500);

            }
        });

        String item = getIntent().getStringExtra("item");
        Log.d(getString(R.string.debug_tag), "Item: " + item);
        if (item != null && item.equals("bag")) {
            setChosenProduct("bag_1.png");
            setSimillarProduct("bag_2.png");
            setCompleteLook("bag_3.png");
        } else if (item != null && item.equals("bag2")) {
            setChosenProduct("bag2_1.png");
            setSimillarProduct("bag2_2.png");
            setCompleteLook("bag2_3.png");
        } else if (item != null && item.equals("bag3")) {
            setChosenProduct("bag3_1.png");
            setSimillarProduct("bag3_2.png");
            setCompleteLook("bag3_3.png");
        } else if (item != null && item.equals("bag4")) {
            setChosenProduct("bag4_1.png");
            setSimillarProduct("bag4_2.png");
            setCompleteLook("bag4_3.png");
        } else if (item != null && item.equals("bag5")) {
            setChosenProduct("bag4_1.png");
            setSimillarProduct("bag4_2.png");
            setCompleteLook("bag4_3.png");
        }
        else {
            setChosenProduct("women_dress_1.png");
            setSimillarProduct("women_dress_2.png");
            setCompleteLook("women_dress_3.png");
        }

    }

    private void setCompleteLook(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivCompleteLook.setImageBitmap(bitmap);
    }

    private void setSimillarProduct(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivSimilarDress.setImageBitmap(bitmap);
        ivSimilarDress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChosenProduct("similar_women_dress_1.png");
                setSimillarProduct("similar_women_dress_2.png");
                setCompleteLook("similar_women_dress_3.png");
            }
        });
    }

    private void setChosenProduct(String fileName) {
        filename = retailApparelFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivDress.setImageBitmap(bitmap);
    }
}
