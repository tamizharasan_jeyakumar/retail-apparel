package com.wipro.technovation.retailapparel;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;

public class Splash extends AppCompatActivity implements NdefReaderTask.AsyncResponse {

    private static final int MY_PERMISSIONS_REQUEST = 1;

    private NfcAdapter mNfcAdapter;
    public static final String MIME_TEXT_PLAIN = "text/plain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        String action = getIntent().getAction();
        Log.d(getString(R.string.debug_tag), "App action: " + action);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }


        handleIntent(getIntent());


        //for status bar color
        Window window = Splash.this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                // finally change the color
                window.setStatusBarColor(Splash.this.getResources().getColor(R.color.colorPrimary));
            }

        }

        File retailFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo");
        if (!retailFileRoot.exists()) {
            retailFileRoot.mkdirs();
        }

        File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Apparel");
        if (!retailApparelFileRoot.exists()) {
            retailApparelFileRoot.mkdirs();
        }
    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(getString(R.string.debug_tag), "Explicit permission not granted!");
            ActivityCompat.requestPermissions(Splash.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST);
        } else {
            startMainActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(getString(R.string.debug_tag), "Permission Granted");
            startMainActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        setupForegroundDispatch(this, mNfcAdapter);
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(this);

    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        stopForegroundDispatch(this, mNfcAdapter);
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(null);

        super.onPause();
    }


    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }


    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        Log.d(getString(R.string.debug_tag), "TAG Detect!");
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        Log.d(getString(R.string.debug_tag), "App action handleIntent: " + action);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask(Splash.this).execute(tag);

            } else {
                Log.d(getString(R.string.debug_tag), "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask(Splash.this).execute(tag);
                    break;
                }
            }
        } else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 2000ms
                    if (Build.VERSION.SDK_INT >= 23) {
                        checkForPermissions();
                    } else {
                        Log.d(getString(R.string.debug_tag), "Explicit permissions not needed");
                        startMainActivity();
                    }
                }
            }, 2500);

        }
    }

    @Override
    public void processFinish(String output) {
        Log.d(getString(R.string.debug_tag), "NFC Data Returned: " + output);
        startActivity(new Intent(Splash.this, WomenDress.class).putExtra("item", output));
        finish();

    }

    private void startMainActivity() {
//        Intent service = new Intent(Splash.this, BluetoothService.class);
//        if (!isMyServiceRunning(BluetoothService.class, Splash.this)) {
//            service.setAction(STARTFOREGROUND_ACTION);
//            Splash.this.startService(service);
//            Log.d(getString(R.string.debug_tag), "Bluetooth service started from splash screen");
//        }

        startActivity(new Intent(Splash.this, RangingActivity.class));
        finish();
    }
}
